<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "employees".
 *
 * @property int $id
 * @property string $employee
 * @property string $date
 * @property string $time_start
 * @property string $time_end
 * @property string $task_description
 */
class Employees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee', 'date', 'time_start', 'time_end', 'task_description'], 'required'],
            [['date', 'time_start', 'time_end'], 'safe'],
            [['task_description'], 'string'],
            [['employee'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee' => 'Employee',
            'date' => 'Date',
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'task_description' => 'Task Description',
        ];
    }
}
