<?php

use backend\models\Employees;
use yii\db\Migration;

/**
 * Class m200223_143848_employees
 */
class m200223_143848_employees extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%employees}}', [
            'id' => $this->primaryKey(),
            'employee' => $this->string()->notNull(),
            'date' => $this->integer(),
            'time_start' => $this->integer(),
            'time_end' => $this->integer(),
            'task_description' => $this->text()
        ]);


        // add new default data
        $this->insert('{{%employees}}',  [
            'id' => 1,
            'employee' => 'vladimir kjahrenov',
            'date' => time(),
            'time_start' => time(),
            'time_end' => time(),
            'task_description' => 'this is test description'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        //echo "m200223_143848_employees cannot be reverted.\n";
        $this->dropTable('{{%employees}}');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200223_143848_employees cannot be reverted.\n";

        return false;
    }
    */
}
