<p align="center">
    <h1 align="center">RESTful demo project for Berkle IT</h1>
    <br>
</p>

This Project is made for Berkle IT by Vladimir Kjahrenov.

 
Project includes three tiers: 
* front end
* back end
* RESTful 

additionally you can use console for running application istallation and running UNIT tests 


DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
api
    config/              contains API configurations
    modules/v1/          containes all modules as RESTful api versions, each version in own folder
        controlles/      contains API controller classes
        models/          contains API-specific model classes
    runtime/             contains files generated during runtime
    web/                 contains the entry script and Web resources
```


# INSTALLATION

* clone project from gitlab: ```git clone https://gitlab.com/hurtlockerpro/berkleit.git```
* open CMD or Git BASH and go to the project forlder 
* run composer to install all vendor packages and libraries: ```composer install``` or ```composer update```
* run command ```php init```
* Create new database in local/remote machine and name it as: ```berkleit```
* Change database config file common/config/main-local.php. Find DSN row : 'dsn' => 'mysql:host=localhost;dbname=berkleit' if you have created database with different name then change name here. Delete text ```berkleit``` and insert your database name. In the same file change username and password so application can login to database.
* open CMD and go to the project folder and run: ```yii migrate``` This will install all required tables and default data such as admin user
* Create 3 virtual hosts On local machine:
    * frontend.test -> set absolute path of the VirtualHost folder Example: {path_to_www_foler}/frontend/web
    * backend.test -> set absolute path of the VirtualHost folder Example: {path_to_www_foler}/backend/web
    * berkleapi.ee -> set absolute path of the VirtualHost folder Example: {path_to_www_foler}/api/web
    
**NB! if hosts were not created then project will not work properly**

If you dont know how to make virtual hosts try this:
On Apache 'bin\apache\apache2.4.27\conf\extra\ create new file 'httpd-vhosts.conf' and insert text below (chanhe the paths to projects)

```
 <VirtualHost *:80>
 	ServerName berkleapi.ee
 	DocumentRoot "c:/serverphp/www/berkleita/api/web"
 	<Directory  "c:/serverphp/www/berkleita/api/web/">
 		Options +Indexes +Includes +FollowSymLinks +MultiViews
 		AllowOverride All
 		Require local
 	</Directory>
 </VirtualHost>
 <VirtualHost *:80>
 	ServerName frontend.test
 	DocumentRoot "c:/serverphp/www/berkleita/frontend/web"
 	<Directory  "c:/serverphp/www/berkleita/frontend/web/">
 		Options +Indexes +Includes +FollowSymLinks +MultiViews
 		AllowOverride All
 		Require local
 	</Directory>
 </VirtualHost>
 <VirtualHost *:80>
 	ServerName backend.test
 	DocumentRoot "c:/serverphp/www/berkleita/backend/web"
 	<Directory  "c:/serverphp/www/berkleita/backend/web/">
 		Options +Indexes +Includes +FollowSymLinks +MultiViews
 		AllowOverride All
 		Require local
 	</Directory>
 </VirtualHost>
```
    
    after creation restart Apache and all services
    
# HOW TO USE FRONTEND

* Open frontend.test 

In frontend you can list all employees and create new ones without login. 

NB! RESTful API need to be run in ```berkleapi.ee``` !!! because ALL CHECKS will be done by sending queries to RESTful API service !!! 
So if service not working properly then no errors will shown during new employee time insertions
frontend.test/index also uses RESTful service to show list of employees

# HOW TO USE BACKEND
* open backend.test

**NB! to use admin backend you need to login as admin! use default username: admin and password: admin**

*in backend you can delete/update/create new employee time records into database

# HOW TO USE RESTful 
* main URL (if virtual host are made) is ```berkleapi.ee```
* You can use postman [https://www.postman.com/] to check all queries 
* you can get list of all employees by sending in postman ```GET http://berkleapi.ee/v1/employees```
* if you want to get data filtered then send: ```GET http://berkleapi.ee/v1/employees?employee=kjahrenov```
* Service will return data in json format 

# CORS problem and errors 
If you set all virtual hosts on one local machine then could happen some errors then using RESTful service. CORS error could happen. I'm using Google Chrome browser and simpliest and quickest way to fix this type of error is to install CORS plugin and turn it on !
In order to download and use an add-on, please visit the official store for each browser:
* https://addons.mozilla.org/en-US/firefox/
* https://chrome.google.com/webstore/category/extensions
* https://addons.opera.com/en/
* https://apps.apple.com/us/story/id1377753262

and use the provided download links in order to add the extension/app to your browser. 
 Name is: CORS: Access-Control-Allow-origin

# UNITtesting
You can run some tests using Codeception. 
* create another database with name ```berkleit_test```
* run command ``` yii_test migrate``` This will create same structure for testing.
* Go to the folder vendor\bin and run command: 
    * ```codecept run -- -c ../../frontend```
    * ```codecept run -- -c ../../backend```

# TECHNOLIGIES USED
* PHP 7.1
* Yii 2.0 framework -> [www.yiiframework.com]
* RESTful 
* Bootstrap 4
* jQuery 3.4 -> [https://jquery.com/]
* Codeception [https://codeception.com/]

Some time Plugins made by Kartik
* Date Picker [https://demos.krajee.com/widget-details/datepicker]
* Time picker [https://demos.krajee.com/widget-details/timepicker]

All required packages will be installed by composer ```composer install``` or ```composer update```

# CONTACT
* Vladimir Kjahrenov 
* info@hurtlocker.pro
* Tel. +372 55 621 935
* Skype: hurtlockerpro
