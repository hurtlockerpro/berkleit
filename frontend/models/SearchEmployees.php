<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Employees;

/**
 * SearchEmployees represents the model behind the search form of `app\models\Employees`.
 */
class SearchEmployees extends Employees
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'date', 'time_start', 'time_end'], 'integer'],
            [['employee', 'task_description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employees::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'time_start' => $this->time_start,
            'time_end' => $this->time_end,
        ]);

        $query->andFilterWhere(['like', 'employee', $this->employee]);

        return $dataProvider;
    }


    public function searchByTime($params)
    {
        $query = Employees::find();

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        /*
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        */

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'date' => $this->date,
            // 'time_start' => $this->time_start,
            //'time_end' => $this->time_end,
        ]);

        $query->andFilterWhere(['<=', 'time_start', $this->time_start])
            ->andFilterWhere(['>=', 'time_end', $this->time_start]);

        //$query->andFilterWhere(['like', 'employee', $this->employee]);

        return $dataProvider;
    }
}
