<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>This project is made by <b>Vladimir Kjahrenov</b>. You can freely contact me by <br><br>Gsm: +372 55 621 935 <br>Skype: <a href="skype:enerboy">enerboy</a> <br>
        E-mail: <a href="mailto:info@hurtlocker.pro">info@hurtlocker.pro</a></p>

</div>
