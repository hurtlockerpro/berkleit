
$(document).ready(function(){

    console.log( "ready!" );



});


function checkTime(e)
{
    //console.log("timeStamp: " + e);

    var date = $("#employees-date").val();
    var tmpDate = date.split(".");

    var time = $("#employees-time_start").val();
    var tmpTime = time.split(':');

    var chosenDate = new Date(tmpDate[2], tmpDate[1]-1, tmpDate[0], 2, 0, 0);
    var chosenTime = new Date(tmpDate[2], tmpDate[1]-1, tmpDate[0], parseInt(tmpTime[0]) + 2, tmpTime[1], 0);


    // console.log('chosenDate: ' + chosenDate);
    // console.log('chosenTime: ' + chosenTime);
    console.log('http://berkleapi.ee/v1/employees?date=' + (chosenDate.getTime() / 1000) + '&time_start=' + (chosenTime.getTime() / 1000));


    $.ajax('http://berkleapi.ee/v1/employees?date=' + (chosenDate.getTime() / 1000) + '&time_start=' + (chosenTime.getTime() / 1000),
        {
            dataType: 'json', // type of response data
            timeout: 500,     // timeout milliseconds
            success: function (data, status, xhr) {   // success callback function

                if (data.length > 0) {
                    $('#result').html('You can\'t save this time! Another user use it: ' + data[0].employee + ': ' + data[0].date + ', ' + data[0].time_start + ' - ' + data[0].time_end);

                    $('#result').removeClass('success');
                    $('#result').addClass('fail');

                    $('#btnSubmit').attr("disabled", true);

                } else {
                    $('#result').html('Everithing is OK');

                    $('#result').removeClass('fail');
                    $('#result').addClass('success');

                    $('#btnSubmit').attr("disabled", false);
                }
                console.log(data);

            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback
                $('#result').html('Error: ' + errorMessage);
                console.log("Error occured");
            }
        });

}
