<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "employees".
 *
 * @property int $id
 * @property string $employee
 * @property string $date
 * @property string $time_start
 * @property string $time_end
 * @property string $task_description
 */
class Employees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee', 'date', 'time_start', 'time_end', 'task_description'], 'required'],
            [['date', 'time_start', 'time_end'], 'safe'],
            [['task_description'], 'string'],
            [['employee'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee' => 'Employee',
            'date' => 'Date',
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'task_description' => 'Task Description',
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->date = strtotime($this->date);

        $timeS = explode(':', $this->time_start);
        $timeE = explode(':', $this->time_end);

        $this->time_start = mktime($timeS[0], $timeS[1], 0, date('m', $this->date), date('d', $this->date), date('Y', $this->date));
        $this->time_end = mktime($timeE[0], $timeE[1], 0, date('m', $this->date), date('d', $this->date), date('Y', $this->date));

        return true;
    }

    public function afterFind()
    {
        $this->time_start = Yii::$app->formatter->asTime($this->time_start);
        $this->time_end = Yii::$app->formatter->asTime($this->time_end);
        $this->date = date('d.m.Y', $this->date);

        return parent::afterFind();
    }
}
