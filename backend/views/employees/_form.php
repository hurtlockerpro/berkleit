<?php

use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employees-form">

    <div id="result"></div>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'employee')->textInput(['maxlength' => true]) ?>

    <?php

    echo $form->field($model, 'date')->widget(DatePicker::className(), [
        'name' => 'date',
        'value' => date('d.m.Y', strtotime($model->date)),
        'convertFormat' => true,
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'Select date ...'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.MM.yyyy',
                'todayHighlight' => true,
            ],
            'pluginEvents' => [
                //"changeDate" => "function(e) { alert('date changed') }",
            ]
        ]);

    echo $form->field($model, 'time_start')->widget(TimePicker::classname(), [
        'name' => 'time_start',
        //'value' => '11:24 AM',
        'value' => Yii::$app->formatter->asTime($model->time_start),
        'pluginOptions' => [
            'showSeconds' => false,
            'minuteStep' => 15,
            'showMeridian' => false,
        ],
        'pluginEvents' => [
            "change" => "function(e) { 
            
               // console.log(e);
               checkTime(e);

            }",
        ]
    ]);

    echo $form->field($model, 'time_end')->widget(TimePicker::classname(), [
            'name' => 'time_end',
          //  'value' => '11:24 AM',
            'pluginOptions' => [
                'showSeconds' => false,
                'minuteStep' => 15,
                'showMeridian' => false,
            ]
        ]);
    ?>

    <?= $form->field($model, 'task_description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
